package ratna.knowme;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Editprofile extends AppCompatActivity {
    ImageView view_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        view_img= (ImageView) findViewById(R.id.chng_img);
        view_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(Editprofile.this);
        dialog.setContentView(R.layout.change_photo_dialog);
        dialog.getWindow().setLayout((int) (getScreenWidth(this) * .9), ViewGroup.LayoutParams.MATCH_PARENT);
        // dialog.getWindow().setLayout((int) (getScreenHeight(this) * .8), ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();

    }
    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }
}
